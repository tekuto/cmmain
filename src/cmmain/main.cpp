﻿#include <libgen.h>
#include <dlfcn.h>
#include <string>
#include <cstring>
#include <memory>
#include <utility>

#ifdef  DEBUG
#   include <cstdio>
#endif  // DEBUG

namespace {
    const auto  LIBRARY_PREFIX = "lib";
    const auto  LIBRARY_SUFFIX = ".so";

    const auto  CURRENT_DIR = '.';
    const auto  DIR_SEPARATOR = '/';

    const auto  CMMAIN_SYMBOL = "_ZN10candymaker4mainEiPPc";

    struct CloseHandle
    {
        void operator()(
            void *  _handlePtr
        ) const
        {
            dlclose( _handlePtr );
        }
    };

    typedef std::unique_ptr<
        void
        , CloseHandle
    > HandleUnique;

    typedef bool( * CmMain )(
        int
        , char **
    );

    bool initLibraryName(
        std::string &           _libraryName
        , const std::string &   _EXECUTE_PATH
    )
    {
        auto    buffer = _EXECUTE_PATH;

        const auto  EXECUTE_NAME = basename( &( buffer[ 0 ] ) );

        auto    libraryName = std::string();
        libraryName.append( LIBRARY_PREFIX );
        libraryName.append( EXECUTE_NAME );
        libraryName.append( LIBRARY_SUFFIX );

        _libraryName = std::move( libraryName );

        return true;
    }

    bool isCurrentDir(
        const std::string & _DIR
    )
    {
        if( _DIR.size() != 1 ) {
            return false;
        }

        if( _DIR[ 0 ] != CURRENT_DIR ) {
            return false;
        }

        return true;
    }

    bool isCurrentPath(
        const std::string & _PATH
    )
    {
        if( _PATH.size() < 2 ) {
            return false;
        }

        if( _PATH[ 0 ] != CURRENT_DIR ) {
            return false;
        }

        if( _PATH[ 1 ] != DIR_SEPARATOR ) {
            return false;
        }

        return true;
    }

    bool initLibraryDir(
        std::string &           _libraryDir
        , const std::string &   _EXECUTE_PATH
    )
    {
        auto    buffer = _EXECUTE_PATH;

        const auto  EXECUTE_DIR = dirname( &( buffer[ 0 ] ) );

        auto    libraryDir = std::string();

        if( isCurrentDir( EXECUTE_DIR ) == true && isCurrentPath( _EXECUTE_PATH ) == false ) {
        } else {
            libraryDir.append( EXECUTE_DIR );
        }

        _libraryDir = std::move( libraryDir );

        return true;
    }

    bool initLibraryPath(
        std::string &           _libraryPath
        , const std::string &   _EXECUTE_PATH
    )
    {
        auto    libraryName = std::string();
        if( initLibraryName(
            libraryName
            , _EXECUTE_PATH
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ライブラリ名の初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    libraryDir = std::string();
        if( initLibraryDir(
            libraryDir
            , _EXECUTE_PATH
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ライブラリディレクトリの初期化に失敗\n" );
#endif  // DEBUG

            return false;
        }

        auto    libraryPath = std::string();
        if( libraryDir.length() > 0 ) {
            libraryPath.append( libraryDir );
            libraryPath.push_back( DIR_SEPARATOR );
        }

        libraryPath.append( libraryName );

        _libraryPath = std::move( libraryPath );

        return true;
    }

    void * newHandle(
        const std::string & _EXECUTE_PATH
    )
    {
        auto    libraryPath = std::string();
        if( initLibraryPath(
            libraryPath
            , _EXECUTE_PATH
        ) == false ) {
#ifdef  DEBUG
            std::printf( "E:ライブラリパスの初期化に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        auto    thisUnique = HandleUnique(
            dlopen(
                libraryPath.c_str()
                , RTLD_LAZY | RTLD_GLOBAL
            )
        );
        if( thisUnique.get() == nullptr ) {
#ifdef  DEBUG
            std::printf( "E:モジュールのオープンに生成に失敗\n" );
#endif  // DEBUG

            return nullptr;
        }

        return thisUnique.release();
    }

    bool getProc(
        void * &        _proc
        , void *        _handlePtr
        , const char *  _SYMBOL
    )
    {
        dlerror();
        const auto  PROC = dlsym(
            _handlePtr
            , _SYMBOL
        );
        const auto  ERROR = dlerror();
        if( ERROR != nullptr ) {
#ifdef  DEBUG
            std::printf(
                "E:シンボルに対応する関数の取得に失敗\"%s\"\n"
                , ERROR
            );
#endif  // DEBUG

            return false;
        }

        _proc = PROC;

        return true;
    }

    template< typename PROC_T >
    bool getProc(
        PROC_T &        _proc
        , void *        _handlePtr
        , const char *  _SYMBOL
    )
    {
        return getProc(
            reinterpret_cast< void * & >( _proc )
            , _handlePtr
            , _SYMBOL
        );
    }
}

int main(
    int         _argc
    , char **   _argv
)
{
    const auto  EXECUTE_PATH = std::string( _argv[ 0 ] );

    auto    handleUnique = HandleUnique( newHandle( EXECUTE_PATH ) );
    if( handleUnique.get() == nullptr ) {
#ifdef  DEBUG
        std::printf( "E:モジュールハンドルの生成に失敗\n" );
#endif  // DEBUG

        return 1;
    }
    auto    handlePtr = handleUnique.get();

    auto    cmMain = CmMain();
    if( getProc(
        cmMain
        , handlePtr
        , CMMAIN_SYMBOL
    ) == false ) {
#ifdef  DEBUG
        std::printf( "E:関数の取得に失敗\n" );
#endif  // DEBUG

        return 1;
    }

    if( cmMain(
        _argc
        , _argv
    ) == false ) {
        return 1;
    }

    return 0;
}
