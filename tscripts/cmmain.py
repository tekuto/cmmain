# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.BUILDER = cpp.program

module.TARGET = 'candymaker'

module.SOURCE = [
    'main.cpp',
]

module.LIB = [
    'dl',
]
