# -*- coding: utf-8 -*-

from taf import *
from taf.tools import cpp

module.TYPE = module.test

module.BUILDER = cpp.shlib

module.TARGET = 'candymaker'

module.SOURCE = [
    'dummylib.cpp',
]
